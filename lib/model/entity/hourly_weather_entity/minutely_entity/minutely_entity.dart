import 'package:json_annotation/json_annotation.dart';
part 'minutely_entity.g.dart';
@JsonSerializable()
class MinutelyEntity {
  @JsonKey(name: 'dt')
  final int? dt;

  @JsonKey(name: 'precipitation')
  final int? precipitation;


  MinutelyEntity({this.dt, this.precipitation});

  factory MinutelyEntity.fromJson(Map<String, dynamic> json) =>
      _$MinutelyEntityFromJson(json);

  Map<String, dynamic> toJson() => _$MinutelyEntityToJson(this);
}
