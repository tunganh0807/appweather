import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast/model/entity/hourly_weather_entity/current_entity/current_entity.dart';
import 'package:weather_forecast/model/entity/hourly_weather_entity/minutely_entity/minutely_entity.dart';

part 'hourly_weather_entity.g.dart';
@JsonSerializable()
class HourlyWeatherEntity {
  @JsonKey(name: 'lat')
  final double? lat;

  @JsonKey(name: 'lon')
  final double? lon;

  @JsonKey(name: 'timezone')
  final String? timezone;

  @JsonKey(name: 'timezone_offset')
  final int? timezoneOffset;

  @JsonKey(name: 'current')
  final CurrentEntity? current;

  @JsonKey(name: 'minutely')
  final List<MinutelyEntity>? minutely;

  @JsonKey(name: 'hourly')
  final List<CurrentEntity>? hourly;

  HourlyWeatherEntity(
      {this.lat,
      this.lon,
      this.timezone,
      this.timezoneOffset,
      this.current,
      this.minutely,
      this.hourly});

  factory HourlyWeatherEntity.fromJson(Map<String, dynamic> json) =>
      _$HourlyWeatherEntityFromJson(json);

  Map<String, dynamic> toJson() => _$HourlyWeatherEntityToJson(this);
}
