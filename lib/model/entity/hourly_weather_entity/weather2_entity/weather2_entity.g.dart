// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather2_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Weather2Entity _$Weather2EntityFromJson(Map<String, dynamic> json) =>
    Weather2Entity(
      id: json['id'] as int?,
      main: $enumDecodeNullable(_$MainEnumEnumMap, json['main']),
      description:
          $enumDecodeNullable(_$DescriptionEnumEnumMap, json['description']),
      icon: $enumDecodeNullable(_$IconEnumEnumMap, json['icon']),
    );

Map<String, dynamic> _$Weather2EntityToJson(Weather2Entity instance) =>
    <String, dynamic>{
      'id': instance.id,
      'main': _$MainEnumEnumMap[instance.main],
      'description': _$DescriptionEnumEnumMap[instance.description],
      'icon': _$IconEnumEnumMap[instance.icon],
    };

const _$MainEnumEnumMap = {
  MainEnum.CLEAR: 'CLEAR',
  MainEnum.CLOUDS: 'CLOUDS',
};

const _$DescriptionEnumEnumMap = {
  DescriptionEnum.CLEAR_SKY: 'CLEAR_SKY',
  DescriptionEnum.FEW_CLOUDS: 'FEW_CLOUDS',
  DescriptionEnum.SCATTERED_CLOUDS: 'SCATTERED_CLOUDS',
  DescriptionEnum.BROKEN_CLOUDS: 'BROKEN_CLOUDS',
  DescriptionEnum.OVERCAST_CLOUDS: 'OVERCAST_CLOUDS',
};

const _$IconEnumEnumMap = {
  IconEnum.THE_01_D: 'THE_01_D',
  IconEnum.THE_01_N: 'THE_01_N',
  IconEnum.THE_02_N: 'THE_02_N',
  IconEnum.THE_03_D: 'THE_03_D',
  IconEnum.THE_04_D: 'THE_04_D',
  IconEnum.THE_04_N: 'THE_04_N',
};
