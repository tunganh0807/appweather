import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast/model/enum/description_enum.dart';
import 'package:weather_forecast/model/enum/icon_enum.dart';
import 'package:weather_forecast/model/enum/main_enum.dart';

part 'weather2_entity.g.dart';
@JsonSerializable()
class Weather2Entity {
  @JsonKey(name: 'id')
  final int? id;

  @JsonKey(name: 'main')
 final MainEnum? main;

  @JsonKey(name: 'description')
  final DescriptionEnum? description;

  @JsonKey(name: 'icon')
  final IconEnum? icon;


  Weather2Entity({this.id, this.main, this.description, this.icon});

  factory Weather2Entity.fromJson(Map<String, dynamic> json) =>
      _$Weather2EntityFromJson(json);

  Map<String, dynamic> toJson() => _$Weather2EntityToJson(this);
}
