// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hourly_weather_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HourlyWeatherEntity _$HourlyWeatherEntityFromJson(Map<String, dynamic> json) =>
    HourlyWeatherEntity(
      lat: (json['lat'] as num?)?.toDouble(),
      lon: (json['lon'] as num?)?.toDouble(),
      timezone: json['timezone'] as String?,
      timezoneOffset: json['timezone_offset'] as int?,
      current: json['current'] == null
          ? null
          : CurrentEntity.fromJson(json['current'] as Map<String, dynamic>),
      minutely: (json['minutely'] as List<dynamic>?)
          ?.map((e) => MinutelyEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      hourly: (json['hourly'] as List<dynamic>?)
          ?.map((e) => CurrentEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$HourlyWeatherEntityToJson(
        HourlyWeatherEntity instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lon': instance.lon,
      'timezone': instance.timezone,
      'timezone_offset': instance.timezoneOffset,
      'current': instance.current,
      'minutely': instance.minutely,
      'hourly': instance.hourly,
    };
