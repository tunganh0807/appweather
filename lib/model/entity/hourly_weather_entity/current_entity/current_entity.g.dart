// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentEntity _$CurrentEntityFromJson(Map<String, dynamic> json) =>
    CurrentEntity(
      dt: json['dt'] as int?,
      sunrise: json['sunrise'] as int?,
      sunset: json['sunset'] as int?,
      temp: (json['temp'] as num?)?.toDouble(),
      feelsLike: (json['feelsLike'] as num?)?.toDouble(),
      pressure: json['pressure'] as int?,
      humidity: json['humidity'] as int?,
      dewPoint: (json['dewPoint'] as num?)?.toDouble(),
      uvi: (json['uvi'] as num?)?.toDouble(),
      clouds: json['clouds'] as int?,
      visibility: json['visibility'] as int?,
      windSpeed: (json['windSpeed'] as num?)?.toDouble(),
      windDeg: json['windDeg'] as int?,
      windGust: (json['windGust'] as num?)?.toDouble(),
      weathers: (json['weathers'] as List<dynamic>?)
          ?.map((e) => WeatherEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      pop: (json['pop'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$CurrentEntityToJson(CurrentEntity instance) =>
    <String, dynamic>{
      'dt': instance.dt,
      'sunrise': instance.sunrise,
      'sunset': instance.sunset,
      'temp': instance.temp,
      'feelsLike': instance.feelsLike,
      'pressure': instance.pressure,
      'humidity': instance.humidity,
      'dewPoint': instance.dewPoint,
      'uvi': instance.uvi,
      'clouds': instance.clouds,
      'visibility': instance.visibility,
      'windSpeed': instance.windSpeed,
      'windDeg': instance.windDeg,
      'windGust': instance.windGust,
      'weathers': instance.weathers,
      'pop': instance.pop,
    };
