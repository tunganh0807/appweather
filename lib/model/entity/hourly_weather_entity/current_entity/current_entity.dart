import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/weather_entity/weather_entity.dart';

part 'current_entity.g.dart';

@JsonSerializable()
class CurrentEntity {
  @JsonKey(name: 'dt')
  final int? dt;
  final int? sunrise;
  final int? sunset;
  final double? temp;
  final double? feelsLike;
  final int? pressure;
  final int? humidity;
  final double? dewPoint;
  final double? uvi;
  final int? clouds;
  final int? visibility;
  final double? windSpeed;
  final int? windDeg;
  final double? windGust;
  final List<WeatherEntity>? weathers;
  final double? pop;


  CurrentEntity(
      {this.dt,
      this.sunrise,
      this.sunset,
      this.temp,
      this.feelsLike,
      this.pressure,
      this.humidity,
      this.dewPoint,
      this.uvi,
      this.clouds,
      this.visibility,
      this.windSpeed,
      this.windDeg,
      this.windGust,
      this.weathers,
      this.pop});


  factory CurrentEntity.fromJson(Map<String, dynamic> json) =>
      _$CurrentEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentEntityToJson(this);

}
