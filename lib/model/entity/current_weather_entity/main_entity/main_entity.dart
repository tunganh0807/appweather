import 'package:json_annotation/json_annotation.dart';

part 'main_entity.g.dart';
@JsonSerializable()
class MainEntity {
  @JsonKey(name: "temp")
  final double? temp;

  @JsonKey(name: "feels_like")
  final double? feelsLike;

  @JsonKey(name: "temp_min")
  final double? tempMin;

  @JsonKey(name: "temp_max")
  final double? tempMax;

  @JsonKey(name: "pressure")
  final int? pressure;

  @JsonKey(name: "humidity")
  final  int? humidity;

  @JsonKey(name: "sea_level")
  final int? seaLevel;

  @JsonKey(name: "grnd_level")
  final int? grndLevel;

  MainEntity({
    this.temp,
    this.feelsLike,
    this.tempMin,
    this.tempMax,
    this.pressure,
    this.humidity,
    this.seaLevel,
    this.grndLevel,
  });

  factory MainEntity.fromJson(Map<String, dynamic> json) =>
      _$MainEntityFromJson(json);

  Map<String, dynamic> toJson() => _$MainEntityToJson(this);
}
