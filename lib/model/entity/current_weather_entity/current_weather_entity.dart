
import 'package:json_annotation/json_annotation.dart';
import 'package:weather_forecast/common/app_images.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/clouds_entity/clouds_entity.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/coord_entity/coord_entity.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/main_entity/main_entity.dart';
import 'package:weather_forecast/pages/home_page/entity/item_grid_view_entity.dart';
import 'sys_entity/sys_entity.dart';
import 'weather_entity/weather_entity.dart';
import 'wind_entity/wind_entity.dart';
part 'current_weather_entity.g.dart';
@JsonSerializable()
class CurrentWeatherEntity {

  @JsonKey(name: 'coord')
  final CoordEntity? coord;

  @JsonKey(name: 'weather')
  final List<WeatherEntity>? weathers;

  @JsonKey(name: 'base')
  final String? base;

  @JsonKey(name: 'main')
  final MainEntity? main;

  @JsonKey(name: "visibility")
  final int? visibility;

  @JsonKey(name: "wind")
  final WindEntity? wind;

  @JsonKey(name: "clouds")
  final CloudsEntity? clouds;

  @JsonKey(name: "dt")
  final int? dt;

  @JsonKey(name: "sys")
  final SysEntity? sys;

  @JsonKey(name: "timezone")
  final int? timezone;

  @JsonKey(name: "id")
  final int? id;

  @JsonKey(name: "name")
  final String? name;

  @JsonKey(name: "cod")
  final int? cod;


  CurrentWeatherEntity(
      {this.coord,
      this.weathers,
      this.base,
      this.main,
      this.visibility,
      this.wind,
      this.clouds,
      this.dt,
      this.sys,
      this.timezone,
      this.id,
      this.name,
      this.cod});

  List<ItemGridViewEntity> getListGridView(CurrentWeatherEntity currentWeatherEntity) {
    return [
      ItemGridViewEntity(
          info: 'Wind',
          parameter: '${currentWeatherEntity.wind?.speed} km/h',
          image: AppImages.icWind),
      ItemGridViewEntity(
          info: 'Chance of rain',
          parameter: '${currentWeatherEntity.clouds?.all} %',
          image: AppImages.icCloud),
      ItemGridViewEntity(
          info: 'Pressure',
          parameter: '${currentWeatherEntity.main?.pressure} mbar',
          image: AppImages.icPressure),
      ItemGridViewEntity(
          info: 'Humidity',
          parameter: '${currentWeatherEntity.main?.humidity} %',
          image: AppImages.icHumidity),
    ];
  }

  factory CurrentWeatherEntity.fromJson(Map<String, dynamic> json) =>
      _$CurrentWeatherEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CurrentWeatherEntityToJson(this);
}
