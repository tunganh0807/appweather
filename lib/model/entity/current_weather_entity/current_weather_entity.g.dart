// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_weather_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CurrentWeatherEntity _$CurrentWeatherEntityFromJson(
        Map<String, dynamic> json) =>
    CurrentWeatherEntity(
      coord: json['coord'] == null
          ? null
          : CoordEntity.fromJson(json['coord'] as Map<String, dynamic>),
      weathers: (json['weather'] as List<dynamic>?)
          ?.map((e) => WeatherEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      base: json['base'] as String?,
      main: json['main'] == null
          ? null
          : MainEntity.fromJson(json['main'] as Map<String, dynamic>),
      visibility: json['visibility'] as int?,
      wind: json['wind'] == null
          ? null
          : WindEntity.fromJson(json['wind'] as Map<String, dynamic>),
      clouds: json['clouds'] == null
          ? null
          : CloudsEntity.fromJson(json['clouds'] as Map<String, dynamic>),
      dt: json['dt'] as int?,
      sys: json['sys'] == null
          ? null
          : SysEntity.fromJson(json['sys'] as Map<String, dynamic>),
      timezone: json['timezone'] as int?,
      id: json['id'] as int?,
      name: json['name'] as String?,
      cod: json['cod'] as int?,
    );

Map<String, dynamic> _$CurrentWeatherEntityToJson(
        CurrentWeatherEntity instance) =>
    <String, dynamic>{
      'coord': instance.coord,
      'weather': instance.weathers,
      'base': instance.base,
      'main': instance.main,
      'visibility': instance.visibility,
      'wind': instance.wind,
      'clouds': instance.clouds,
      'dt': instance.dt,
      'sys': instance.sys,
      'timezone': instance.timezone,
      'id': instance.id,
      'name': instance.name,
      'cod': instance.cod,
    };
