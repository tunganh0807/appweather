import 'package:weather_forecast/common/app_images.dart';

enum IconEnum { THE_01_D, THE_01_N, THE_02_N, THE_03_D, THE_04_D, THE_04_N}

extension IconExtension on IconEnum{
  static IconEnum getIconEnum(String value){
    switch(value){
      case "01d":
        return IconEnum.THE_01_D;
      case "01n":
        return IconEnum.THE_01_N;
      case "02n":
        return IconEnum.THE_02_N;
      case "03d":
        return IconEnum.THE_03_D;
      case "04n":
        return IconEnum.THE_04_N;
      case "04d":
        return IconEnum.THE_04_D;
      default:
        return IconEnum.THE_01_D;
    }
  }


}
