enum MainEnum { CLEAR, CLOUDS }

extension MainEnumExtension on MainEnum{
  static MainEnum getMainEnum(String value){
    switch(value){
      case "Clear":
        return MainEnum.CLEAR;
      case "Clouds":
        return MainEnum.CLOUDS;

      default:
        return MainEnum.CLOUDS;
    }
  }
}