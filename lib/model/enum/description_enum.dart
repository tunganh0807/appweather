
import 'package:weather_forecast/common/app_images.dart';

enum DescriptionEnum {
  CLEAR_SKY, FEW_CLOUDS, SCATTERED_CLOUDS, BROKEN_CLOUDS, OVERCAST_CLOUDS }

extension DescriptionEnumExtension on DescriptionEnum{
  static DescriptionEnum getDescriptionEnum(String value){
    switch(value){
      case "broken clouds":
        return DescriptionEnum.BROKEN_CLOUDS;
      case "clear sky":
        return DescriptionEnum.CLEAR_SKY;
      case "few clouds":
        return  DescriptionEnum.FEW_CLOUDS;
      case "overcast clouds":
        return  DescriptionEnum.OVERCAST_CLOUDS;
      case "scattered clouds":
        return  DescriptionEnum.SCATTERED_CLOUDS;
      default:
        return DescriptionEnum.SCATTERED_CLOUDS;
    }
  }

  String get getIconHourlyWeather{
    switch(this){
      case DescriptionEnum.BROKEN_CLOUDS:
        return AppImages.icThe01D;
      case DescriptionEnum.CLEAR_SKY:
        return AppImages.icThe01N;
      case DescriptionEnum.FEW_CLOUDS:
        return AppImages.icThe02N;
      case DescriptionEnum.SCATTERED_CLOUDS:
        return AppImages.icThe03D;
      case DescriptionEnum.OVERCAST_CLOUDS:
        return AppImages.icThe04D;
      default:
        return AppImages.icThe01D;
    }
  }
}