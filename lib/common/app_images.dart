class AppImages {
  static const icAdd = 'assets/images/ic_add.png';
  static const icPressure = 'assets/images/ic_pressure.png';
  static const icHumidity = 'assets/images/ic_humidity.png';
  static const icWind = 'assets/images/ic_wind.png';
  static const icCloud = 'assets/images/ic_cloud.png';
  static const imgWeather = 'assets/images/img_weather.png';
  static const icOverflowMenu = 'assets/images/ic_overflow_menu.png';
  static const icSlideDot = 'assets/images/ic_slide_dot.png';
  static const icForeCast = 'assets/images/ic_forecast.png';

  static const icThe01D = 'assets/images/ic_the_01_d.png';
  static const icThe01N = 'assets/images/ic_the_01_n.png';
  static const icThe02N = 'assets/images/ic_the_02_n.png';
  static const icThe03D = 'assets/images/ic_the_03_d.png';
  static const icThe04D = 'assets/images/ic_the_04_d.png';
  static const icThe04N = 'assets/images/ic_the_04_n.png';

  static const icDoubleDownArrow = 'assets/images/ic_double_down_arrow.png';
  static const icBackArrow = 'assets/images/ic_back_arrow.png';
  static const icSearch = 'assets/images/ic_search.png';
  static const icLocation = 'assets/images/ic_location.png';

}