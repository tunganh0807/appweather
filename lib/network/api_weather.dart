import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/current_weather_entity.dart';
import 'package:weather_forecast/model/entity/hourly_weather_entity/current_entity/current_entity.dart';
import 'package:weather_forecast/model/entity/hourly_weather_entity/hourly_weather_entity.dart';

class ApiWeather {
  static Future<CurrentWeatherEntity?> getCurrentWeather() async {
    try {
      String url =
          'https://api.openweathermap.org/data/2.5/weather?lat=21.0278&lon=105.8342&appid=97ed9ebd22723116c513b908b11f8085';
      Uri uri = Uri.parse(url);
      Response response = await get(uri);
      //Map<String, String> headers = response.headers;

      if (response.statusCode == 200) {
        dynamic body = jsonDecode(response.body);

        return CurrentWeatherEntity.fromJson(body);
      } else {
        throw "Unable to retrieve posts.";
      }
    } catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }

  static Future<List<CurrentEntity>?> getHourlyWeather() async {
    try {
      String url =
          'https://api.openweathermap.org/data/2.5/onecall?lat=21.0278&lon=105.8342&exclude=daily&appid=82d78aef7a2755507e23056a5b7b885f';
      Uri uri = Uri.parse(url);
      Response response = await get(uri);
      //Map<String, String> headers = response.headers;

      if (response.statusCode == 200) {
        dynamic body = jsonDecode(response.body);

        return (HourlyWeatherEntity.fromJson(body)).hourly;
      } else {
        throw "Unable to retrieve posts.";
      }
    } catch (e) {
      debugPrint(e.toString());
    }
    return null;
  }
}
