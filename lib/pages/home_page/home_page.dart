import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_forecast/common/app_images.dart';
import 'package:weather_forecast/model/entity/current_weather_entity/current_weather_entity.dart';
import 'package:weather_forecast/model/entity/hourly_weather_entity/current_entity/current_entity.dart';
import 'package:weather_forecast/model/enum/description_enum.dart';
import 'package:weather_forecast/network/api_weather.dart';
import 'package:weather_forecast/pages/home_page/entity/item_grid_view_entity.dart';
import 'package:weather_forecast/pages/manage_location_page/manage_location_page.dart';
import 'package:weather_forecast/pages/setting_page/setting_page.dart';
import 'package:weather_forecast/widget/text_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late CurrentWeatherEntity _currentWeatherEntity = CurrentWeatherEntity();
  final ValueNotifier<CurrentWeatherEntity> _currentWeathersNotifier =
      ValueNotifier(CurrentWeatherEntity());
  late bool _isForecastTomorrow = false;
  late final List<CurrentEntity> _listHourlyToday = [];
  late final List<CurrentEntity> _listHourlyTomorrow = [];

  final ValueNotifier<List<CurrentEntity>> _currentNotifier = ValueNotifier([]);
  final ValueNotifier<List<CurrentEntity>> _tomorrowNotifier =
      ValueNotifier([]);

  List<CurrentEntity> _listHourlyWeather = [];

  @override
  void initState() {
    super.initState();

    ApiWeather.getCurrentWeather().then((value) {
      if (value != null) {
        _currentWeatherEntity = value;
        _currentWeathersNotifier.value = _currentWeatherEntity;
      }
    });
    ApiWeather.getHourlyWeather().then((value) {
      if (value != null) {
        _listHourlyWeather = value;
        _currentNotifier.value = _listHourlyWeather;
      }
      for (int i = 0; i <= 23; i++) {
        _listHourlyToday.add(_listHourlyWeather[i]);
      }

      for (int i = 24; i <= _listHourlyWeather.length; i++) {
        _listHourlyTomorrow.add(_listHourlyWeather[i]);
      }

      _tomorrowNotifier.value = _listHourlyTomorrow;
    });
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
    _isForecastTomorrow = !_isForecastTomorrow;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ValueListenableBuilder<CurrentWeatherEntity>(
        valueListenable: _currentWeathersNotifier,
        builder: (context, value, child) {
          if (_currentWeathersNotifier.value == CurrentWeatherEntity()) {
            return const CircularProgressIndicator(
              color: Colors.white,
              backgroundColor: Colors.grey,
            );
          }
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.all(16),
                    padding:
                        const EdgeInsets.only(top: 16, left: 16, right: 16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      gradient: const LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xFF62B8F6),
                          Color(0xFF2C79C1),
                        ],
                      ),
                    ),
                    child: Column(
                      children: [
                        _locationAndOption(),
                        _isForecastTomorrow == false
                            ? _forecastBody()
                            : _forecastBodyChanged(),
                        SizedBox(
                          height: _isForecastTomorrow == false ? 16 : 25,
                        ),
                        const Divider(
                          height: 15,
                          thickness: 1,
                          color: Colors.white,
                          indent: 15,
                          endIndent: 15,
                        ),
                        _gridViewForecast(),
                      ],
                    ),
                  ),
                  _listHourlyForecast(),
                  const SizedBox(
                    height: 16,
                  ),
                  _isForecastTomorrow == false
                      ? _forecastForTomorrow()
                      : _forecastForTomorrowChanged(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _forecastForTomorrow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {
            setState(() {});
          },
          child: const TextWidget(
            text: 'Forecast for Tomorrow',
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Color(0xFF2C79C1),
          ),
        ),
        Image.asset(
          AppImages.icDoubleDownArrow,
          width: 24,
          height: 24,
        )
      ],
    );
  }

  Widget _forecastForTomorrowChanged() {
    return Container(
      padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
      color: const Color(0xFF2C79C1),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              setState(() {});
            },
            child: const Text(
              'Forcats for Tomorrow ',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            height: 250,
            child: ValueListenableBuilder(
              valueListenable: _tomorrowNotifier,
              builder: (context, value, child) {
                return ListView.separated(
                  padding: const EdgeInsets.only(left: 5),
                  itemCount: _listHourlyTomorrow.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return _itemForecastTomorrow(
                        _listHourlyTomorrow[index], index);
                  },
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 5),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _itemForecastTomorrow(CurrentEntity? hourlyTomorrow, int index) {
    DescriptionEnum descriptionEnum =
        DescriptionEnumExtension.getDescriptionEnum(
            (hourlyTomorrow?.weathers ?? []).isNotEmpty
                ? hourlyTomorrow!.weathers!.first.description ?? ''
                : '');
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: Row(
        children: [
          const SizedBox(
            width: 12,
          ),
          Expanded(
            flex: 3,
            child: Text(
              '$index:00',
              style: const TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500),
            ),
          ),
          Expanded(
            flex: 5,
            child: Row(
              children: [
                Image.asset(
                  descriptionEnum.getIconHourlyWeather,
                  width: 20,
                  height: 20,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  '${(hourlyTomorrow?.clouds.toString() ?? '')}% rain',
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              '${((hourlyTomorrow?.temp ?? 0) - 273).toString().substring(0, 2)}°',
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
  }

  Widget _forecastBody() {
    return Column(
      children: [
        Image.asset(
          AppImages.imgWeather,
          height: 0.3 * MediaQuery.of(context).size.height,
          width: 0.6 * MediaQuery.of(context).size.width,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 100, right: 100),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextWidget(
                  text: DateFormat('EEEE').format(DateTime.now()),
                  fontSize: 16,
                  fontWeight: FontWeight.w400),
              Container(
                width: 1,
                height: 20,
                color: Colors.white,
              ),
              TextWidget(
                  text: DateFormat.MMMd().format(DateTime.now()),
                  fontSize: 16,
                  fontWeight: FontWeight.w400)
            ],
          ),
        ),
        TextWidget(
          text:
              '${((_currentWeathersNotifier.value.main?.temp ?? 0) - 273).toString().substring(0, 2)}°',
          fontSize: 72,
          fontWeight: FontWeight.w600,
        ),
        TextWidget(
            text: _currentWeathersNotifier.value.weathers?[0].description
                    ?.toUpperCase() ??
                '',
            fontSize: 16,
            fontWeight: FontWeight.w400),
      ],
    );
  }

  Widget _forecastBodyChanged() {
    return Padding(
      padding: const EdgeInsets.only(left: 13, right: 13),
      child: Row(
        children: [
          Image.asset(
            AppImages.imgWeather,
            height: 0.2 * MediaQuery.of(context).size.height,
            width: 0.4 * MediaQuery.of(context).size.width,
          ),
          const SizedBox(
            width: 26,
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    TextWidget(
                        text: DateFormat('EEEE').format(DateTime.now()),
                        fontSize: 16,
                        fontWeight: FontWeight.w400),
                    const SizedBox(
                      width: 10,
                    ),
                    TextWidget(
                        text: DateFormat.MMMd().format(DateTime.now()),
                        fontSize: 16,
                        fontWeight: FontWeight.w400)
                  ],
                ),
                TextWidget(
                  text:
                      '${((_currentWeathersNotifier.value.main?.temp ?? 0) - 273).toString().substring(0, 2)}°',
                  fontSize: 72,
                  fontWeight: FontWeight.w600,
                ),
                TextWidget(
                    text: _currentWeathersNotifier
                            .value.weathers?[0].description
                            ?.toUpperCase() ??
                        '',
                    fontSize: 17,
                    fontWeight: FontWeight.w400),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _locationAndOption() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (_) => const ManageLocationPage()));
          },
          child: Image.asset(
            AppImages.icAdd,
            height: 32,
            width: 32,
          ),
        ),
        Column(
          children: [
            TextWidget(
                text: _currentWeathersNotifier.value.name ?? 'no data',
                fontSize: 20,
                fontWeight: FontWeight.w600),
            Image.asset(
              AppImages.icSlideDot,
              width: 32,
              height: 8,
            )
          ],
        ),
        InkWell(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (_) => const SettingPage()));
          },
          child: Image.asset(
            AppImages.icOverflowMenu,
            height: 30,
            width: 30,
          ),
        )
      ],
    );
  }

  Widget _gridViewForecast() {
    return GridView.builder(
      itemCount: 4,
      shrinkWrap: true,
      padding: const EdgeInsets.only(left: 40),
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 3,
          mainAxisSpacing: 1,
          childAspectRatio: 3.2),
      itemBuilder: (BuildContext context, int index) {
        return _itemGridView(
            list: CurrentWeatherEntity()
                .getListGridView(_currentWeathersNotifier.value),
            index: index);
      },
    );
  }

  Widget _listHourlyForecast() {
    return ValueListenableBuilder(
      valueListenable: _currentNotifier,
      builder: ((context, value, child) {
        return Container(
          decoration: const BoxDecoration(
            color: Color(0xFF2C79C1),
          ),
          height: MediaQuery.of(context).size.height * 0.165,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16, top: 8),
                child: Row(
                  children: [
                    TextWidget(
                        text: DateFormat('EEEE').format(DateTime.now()),
                        fontSize: 16,
                        fontWeight: FontWeight.w400),
                    const SizedBox(
                      width: 10,
                    ),
                    TextWidget(
                      text: DateFormat.MMMd().format(DateTime.now()),
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    )
                  ],
                ),
              ),
              _listHourlyWeather.isNotEmpty
                  ? SizedBox(
                      height: 105,
                      child: ListView.separated(
                        itemCount: _listHourlyToday.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return _itemListView(_listHourlyToday[index], index);
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(width: 5),
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
        );
      }),
    );
  }

  Widget _itemListView(CurrentEntity? hourlyWeather, int index) {
    DescriptionEnum descriptionEnum =
        DescriptionEnumExtension.getDescriptionEnum(
            (hourlyWeather?.weathers ?? []).isNotEmpty
                ? hourlyWeather!.weathers!.first.description ?? ''
                : '');
    return SizedBox(
      width: 80,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextWidget(
              text: '$index:00', fontSize: 16, fontWeight: FontWeight.w500),
          Image.asset(
            descriptionEnum.getIconHourlyWeather,
            height: 24,
            width: 24,
            color: Colors.white,
          ),
          Text(
            "${((hourlyWeather?.temp ?? 0) - 273).toString().substring(0, 2)}°",
            style: const TextStyle(
                color: Colors.white,
                fontSize: 12,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w500),
          ),
          Text(
            "${((hourlyWeather?.clouds ?? 0).toString())}% rain",
            style: const TextStyle(
                color: Colors.white,
                fontSize: 12,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  Widget _itemGridView(
      {required List<ItemGridViewEntity> list, required int index}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          Image.asset(
            list[index].image ?? '',
            height: 32,
            width: 32,
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                list[index].parameter ?? '',
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500),
              ),
              Text(
                list[index].info ?? '',
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
