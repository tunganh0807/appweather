import 'package:flutter/material.dart';
import 'package:weather_forecast/common/app_images.dart';
import 'package:weather_forecast/widget/text_widget.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(16),
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF62B8F6),
                Color(0xFF2C79C1),
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(
                      AppImages.icBackArrow,
                      width: 32,
                      height: 32,
                    ),
                  ),
                  const TextWidget(
                      text: 'Setting',
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                  const SizedBox(
                    width: 30,
                  ),
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              const TextWidget(
                  text: 'UNIT', fontSize: 12, fontWeight: FontWeight.w500),
              const SizedBox(
                height: 20,
              ),
              _itemSetting(unitName: 'Temperature unit', measure: '°C'),
              const SizedBox(
                height: 20,
              ),
              _itemSetting(unitName: 'Wind speed unit', measure: 'km/h'),
              const SizedBox(
                height: 20,
              ),
              _itemSetting(
                  unitName: 'Atmospheric pressure unit', measure: 'mbar')
            ],
          ),
        ),
      ),
    );
  }

  Widget _itemSetting({required String unitName, required String measure}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        TextWidget(text: unitName, fontSize: 16, fontWeight: FontWeight.w400),
        TextWidget(text: measure, fontSize: 14, fontWeight: FontWeight.w400)
      ],
    );
  }
}
