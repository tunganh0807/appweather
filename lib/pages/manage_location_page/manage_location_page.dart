import 'package:flutter/material.dart';
import 'package:weather_forecast/common/app_images.dart';
import 'package:weather_forecast/widget/text_widget.dart';

class ManageLocationPage extends StatefulWidget {
  const ManageLocationPage({Key? key}) : super(key: key);

  @override
  State<ManageLocationPage> createState() => _ManageLocationPageState();
}

class _ManageLocationPageState extends State<ManageLocationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SafeArea(
          child: Container(
            margin: const EdgeInsets.all(16),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              gradient: const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF62B8F6),
                  Color(0xFF2C79C1),
                ],
              ),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        AppImages.icBackArrow,
                        width: 32,
                        height: 32,
                      ),
                    ),
                    const TextWidget(
                        text: 'Manage Location',
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                    const SizedBox(),
                  ],
                ),
                const SizedBox(
                  height: 50,
                ),
                TextField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white60,
                      isDense: true,
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 8),
                      prefixIcon: Image.asset(
                        AppImages.icSearch,
                        width: 8,
                        height: 8,
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(color: Colors.white)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(color: Colors.white)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(16),
                          borderSide: const BorderSide(color: Colors.white)),
                      hintText: 'Search Your City',
                      hintStyle: const TextStyle(
                          fontSize: 12,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          color: Colors.grey)),
                ),
                const SizedBox(
                  height: 30,
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: 3,
                    separatorBuilder: (context, index) => const SizedBox(
                      height: 16,
                    ),
                    itemBuilder: (context, index) => _itemListLocation(),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _itemListLocation() {
    return Container(
      padding: const EdgeInsets.all(16),
      height: 0.1 * MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const TextWidget(
                    text: 'Hanoi',
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF1B2541),
                  ),
                  Image.asset(
                    AppImages.icLocation,
                    height: 16,
                    width: 16,
                  ),
                ],
              ),
              const TextWidget(
                text: '20°/24°',
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color(0xFF545B70),
              )
            ],
          ),
          Column(
            children: [
              Image.asset(
                AppImages.icThe01N,
                height: 32,
                width: 32,
              ),
              const TextWidget(
                text: 'Heavy rain',
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color(0xFF545B70),
              )
            ],
          )
        ],
      ),
    );
  }
}
